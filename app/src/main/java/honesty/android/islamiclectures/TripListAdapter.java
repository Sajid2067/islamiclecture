package honesty.android.islamiclectures;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sajid on 10/28/2015.
 */
public class TripListAdapter extends RecyclerView.Adapter<TripListAdapter.ViewHolder> implements Filterable {

    private Activity activity;
    private List<VideoItem> tripLists;
    private List<VideoItem> backupTripLists;
    private LayoutInflater layoutInflater;
    private AppManager appManager;
    private TextView noResultTextView;
    ValueFilter valueFilter;


    public TripListAdapter(Activity activity, List<VideoItem> recyclerViewBaseItems) {
        this.activity = activity;
        backupTripLists = recyclerViewBaseItems;
        this.tripLists = new ArrayList<>(this.backupTripLists);
     //   this.noResultTextView = noResultTextView;
        layoutInflater = (LayoutInflater) this.activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        appManager = new AppManager(this.activity);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView;
        rootView = layoutInflater.inflate(R.layout.video_item, parent, false);
        return new ViewHolder(rootView);
    }

    public void setItems(final List<VideoItem> recyclerViewBaseItems) {
        backupTripLists = recyclerViewBaseItems;
        this.tripLists = new ArrayList<>(this.backupTripLists);
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                if (recyclerViewBaseItems.size() == 0) {
//                    noResultTextView.setVisibility(View.VISIBLE);
//                } else {
//                    noResultTextView.setVisibility(View.GONE);
//                }
            }
        });
    }

    public VideoItem getItem(int position) {
        return tripLists.get(position);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        VideoItem tripList = tripLists.get(position);
        holder.id.setText(tripList.getId());


        //     holder.image.setText(tripList.getThumbnailURL());

        holder.name.setText(tripList.getName());

//        if (getItemCount() - 1 == position) {
//            holder.listDivider.setVisibility(View.GONE);
//        } else {
//            holder.listDivider.setVisibility(View.VISIBLE);
//        }

    }

    @Override
    public Filter getFilter() {
        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }

    @Override
    public int getItemCount() {
        return tripLists != null ? tripLists.size() : 0;
    }




    static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView id;
        private ImageView image;
        // private TextView busTypeTextView;
        private TextView name;

        //  private View listDivider;

        public ViewHolder(View rootView) {
            super(rootView);
            id = (TextView) rootView.findViewById(R.id.leturer_id);
            image = (ImageView) rootView.findViewById(R.id.image);
            //  busTypeTextView = (TextView) rootView.findViewById(R.id.bus_type_text_view);
            name = (TextView) rootView.findViewById(R.id.name);
            //  newTicketPriceTextView = (Button) rootView.findViewById(R.id.ticket_price_new_button);
            // listDivider = (View) itemView.findViewById(R.id.list_divider);
        }
    }

    private class ValueFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {
                ArrayList<VideoItem> filterList = new ArrayList<VideoItem>();
                for (int i = 0; i < backupTripLists.size(); i++) {
                    if ( (backupTripLists.get(i).getName().toUpperCase() )
                            .contains(constraint.toString().toUpperCase())) {

                        VideoItem country = new VideoItem();
                        country.setId(backupTripLists.get(i).getId());
                        country.setName(backupTripLists.get(i).getName());
                        country.setThumbnailURL(backupTripLists.get(i).getThumbnailURL());

                        filterList.add(country);
                    }
                }
                results.count = filterList.size();
                results.values = filterList;
            } else {
                results.count = backupTripLists.size();
                results.values = backupTripLists;
            }
            return results;

        }

        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            tripLists = (ArrayList<VideoItem>) results.values;
            notifyDataSetChanged();
        }

    }

}



