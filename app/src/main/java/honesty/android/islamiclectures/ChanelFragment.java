package honesty.android.islamiclectures;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ChanelFragment extends Fragment {

    private static final String TAG = ChanelFragment.class.getSimpleName();

    //  ObjectRequest<TripUpdateData> tripUpdateObjectRequest;
    //  ObjectRequest<OnlineTripUpdateData> onlineTripUpdateObjectRequest;

    //  private AppController appController = AppController.getInstance();
    //  private DatabaseHelper db;
 //   private TextView totalTripListTextView;
    private GestureDetector mGestureDetector;
    private LinearLayoutManager linearLayoutManager;

    private RecyclerView tripDetailsHistoryRecyclerView;
    //  private ProgressBar tripDetailsHistoryProgressbar;
    private TripListAdapter tripDetailsHistoryListAdapter;
    private List<VideoItem> tripUpdateDetailsLists;
 //   private View tripDetailsHistoryView;
    //  private AppManager appManager;
    // private List<Trips> tempTrips;
    private Integer deleteDuration = 5;
    //  private AlertDialog noInternetAlertDialog;
    //  private AlertDialog errorAlertDialog;
    Button btnCalendar;
    private int updateOnlineTripList = 1;
    SearchView inputSearch;
    protected View rootView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.lecturer, container, false);
        }

        initializeViewComponents();
        inputSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                tripDetailsHistoryListAdapter.getFilter().filter(newText);
                return false;
            }
        });

        return rootView;
    }

    private void initializeViewComponents() {

        tripDetailsHistoryRecyclerView = (RecyclerView) rootView.findViewById(R.id.book_ticket_recycler_view);
        inputSearch=(SearchView)rootView.findViewById(R.id.inputSearch);
        tripDetailsHistoryRecyclerView.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        tripDetailsHistoryRecyclerView.setLayoutManager(linearLayoutManager);
        tripUpdateDetailsLists = new ArrayList<>();
        VideoItem v = new VideoItem();
        v.setId("1");
        v.setName("TheMercifulServant");
        VideoItem v2 = new VideoItem();
        v2.setId("2");
        v2.setName("The Daily Reminder");

        VideoItem v3 = new VideoItem();
        v3.setId("3");
        v3.setName("Al Falaah");
        VideoItem v4 = new VideoItem();
        v4.setId("4");
        v4.setName("Assunnah Trust");

        VideoItem v5 = new VideoItem();
        v5.setId("5");
        v5.setName("Bayyinah Institute");
        VideoItem v6 = new VideoItem();
        v6.setId("6");
        v6.setName("Right Path");

        VideoItem v7 = new VideoItem();
        v7.setId("7");
        v7.setName("Muslim Speakers");
        VideoItem v8 = new VideoItem();
        v8.setId("8");
        v8.setName("Bd Reminder");

        VideoItem v9 = new VideoItem();
        v9.setId("9");
        v9.setName("MinarMedia");

        VideoItem v10 = new VideoItem();
        v10.setId("10");
        v10.setName("Islam - Topic");
        VideoItem v11 = new VideoItem();
        v11.setId("11");
        v11.setName("ZamZamacademy");

        VideoItem v12 = new VideoItem();
        v12.setId("12");
        v12.setName("Be Better Production");
        VideoItem v13 = new VideoItem();
        v13.setId("13");
        v13.setName("Assimalhakeem");

        VideoItem v14 = new VideoItem();
        v14.setId("14");
        v14.setName("Sheikh Tawfique Chowdhury");

        VideoItem v15 = new VideoItem();
        v15.setId("15");
        v15.setName("Yawar Baig & Associates");

        VideoItem v16 = new VideoItem();
        v16.setId("16");
        v16.setName("Reminders From Mufti Menk");
        VideoItem v17 = new VideoItem();
        v17.setId("17");
        v17.setName("Reminders From Hamza Tzortzis");

        VideoItem v18 = new VideoItem();
        v18.setId("18");
        v18.setName("Reminders From Mohamed Hoblos");
        VideoItem v19 = new VideoItem();
        v19.setId("19");
        v19.setName("Majed Mahmoud");

//        VideoItem v20 = new VideoItem();
//        v20.setId("20");
//        v20.setName("Sheikh Tawfique Chowdhury");






        tripUpdateDetailsLists.add(v);
        tripUpdateDetailsLists.add(v2);
        tripUpdateDetailsLists.add(v3);
        tripUpdateDetailsLists.add(v4);
        tripUpdateDetailsLists.add(v5);
        tripUpdateDetailsLists.add(v6);
        tripUpdateDetailsLists.add(v7);
        tripUpdateDetailsLists.add(v8);
        tripUpdateDetailsLists.add(v9);
        tripUpdateDetailsLists.add(v10);
        tripUpdateDetailsLists.add(v11);
        tripUpdateDetailsLists.add(v12);
        tripUpdateDetailsLists.add(v13);
        tripUpdateDetailsLists.add(v14);
        tripUpdateDetailsLists.add(v15);
        tripUpdateDetailsLists.add(v16);
        tripUpdateDetailsLists.add(v17);
        tripUpdateDetailsLists.add(v18);
        tripUpdateDetailsLists.add(v19);
      //  tripUpdateDetailsLists.add(v14);
        Collections.sort(tripUpdateDetailsLists, new Comparator<VideoItem>(){
            public int compare(VideoItem emp1, VideoItem emp2) {
                // ## Ascending order
                return emp1.getName().compareToIgnoreCase(emp2.getName()); // To compare string values
                // return Integer.valueOf(emp1.getId()).compareTo(emp2.getId()); // To compare integer values

                // ## Descending order
                // return emp2.getFirstName().compareToIgnoreCase(emp1.getFirstName()); // To compare string values
                // return Integer.valueOf(emp2.getId()).compareTo(emp1.getId()); // To compare integer values
            }
        });
        tripDetailsHistoryListAdapter = new TripListAdapter(getActivity(), tripUpdateDetailsLists);
        tripDetailsHistoryRecyclerView.setVisibility(View.VISIBLE);
        tripDetailsHistoryRecyclerView.setAdapter(tripDetailsHistoryListAdapter);
      //  totalTripListTextView = (TextView) rootView.findViewById(R.id.total_trip_list_text_view);
        //  tripDetailsHistoryProgressbar = (ProgressBar) findViewById(R.id.book_ticket_progress_bar);
      //  tripDetailsHistoryView = (View) rootView.findViewById(R.id.book_ticket_view);
//        if(tripUpdateDetailsLists.size()!=0) {
//            tripDetailsHistoryView.setVisibility(VISIBLE);
//        }

        mGestureDetector = new GestureDetector(getActivity(), new GestureDetector.SimpleOnGestureListener() {

            @Override
            public boolean onSingleTapUp(MotionEvent e) {


                return true;
            }
        });


        tripDetailsHistoryRecyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                View child = tripDetailsHistoryRecyclerView.findChildViewUnder(e.getX(), e.getY());
                mGestureDetector.setIsLongpressEnabled(true);

                if (child != null && mGestureDetector.onTouchEvent(e)) {
                    int position = tripDetailsHistoryRecyclerView.getChildAdapterPosition(child);
                    VideoItem tripsItem = tripUpdateDetailsLists.get(position);

                    Log.d(TAG, "Position:" + position + " and tempTrips size: " + tripUpdateDetailsLists.size());

                    Intent intent = new Intent(Intent.ACTION_SEARCH);
                    intent.setPackage("com.google.android.youtube");
                    intent.putExtra("query", tripsItem.getName());
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);


                }

                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {
                //  Toast.makeText(MainActivity.this,"Hi",Toast.LENGTH_LONG).show();
            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
                //Toast.makeText(MainActivity.this,"onRequestDisallowInterceptTouchEvent",Toast.LENGTH_LONG).show();
            }
        });

    }
}
