package honesty.android.islamiclectures;

/**
 * Created by TT on 10/10/2016.
 */
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


public class TripHistoryTabsPagerAdapter extends FragmentPagerAdapter {

    FragmentManager fm;

    public TripHistoryTabsPagerAdapter(FragmentManager fm) {
        super(fm);
        this.fm=fm;

    }

    @Override
    public Fragment getItem(int position) {



        switch (position){

            case 0:
                LecturerFragment lecturerFragment =new LecturerFragment();
                return lecturerFragment;

            case 1:
                ChanelFragment chanelFragment =new ChanelFragment();
                return chanelFragment;

//            case 2:
//                LecturerFragment applicationFragment2 =new LecturerFragment();
//                return applicationFragment2;


        }
        return new LecturerFragment();
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        switch (position){
            case 0:
                return "Speakers";
            case 1:
                return "Channels";
//            case 2:
//                return "Settings";

        }
        return "Speakers";
    }
}
