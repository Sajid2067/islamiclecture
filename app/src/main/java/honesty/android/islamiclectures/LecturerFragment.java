package honesty.android.islamiclectures;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static android.view.View.VISIBLE;

public class LecturerFragment extends Fragment {

    private static final String TAG = LecturerFragment.class.getSimpleName();

    //  ObjectRequest<TripUpdateData> tripUpdateObjectRequest;
    //  ObjectRequest<OnlineTripUpdateData> onlineTripUpdateObjectRequest;

    //  private AppController appController = AppController.getInstance();
    //  private DatabaseHelper db;
 //   private TextView totalTripListTextView;
    private GestureDetector mGestureDetector;
    private LinearLayoutManager linearLayoutManager;

    private RecyclerView tripDetailsHistoryRecyclerView;
    //  private ProgressBar tripDetailsHistoryProgressbar;
    private TripListAdapter tripDetailsHistoryListAdapter;
    private List<VideoItem> tripUpdateDetailsLists;
 //   private View tripDetailsHistoryView;
    //  private AppManager appManager;
    // private List<Trips> tempTrips;
    private Integer deleteDuration = 5;
    //  private AlertDialog noInternetAlertDialog;
    //  private AlertDialog errorAlertDialog;
    Button btnCalendar;
    private int updateOnlineTripList = 1;

    protected View rootView;
    // Search EditText
    SearchView inputSearch;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.lecturer, container, false);
        }

        initializeViewComponents();

//        inputSearch.addTextChangedListener(new TextWatcher() {
//
//            @Override
//            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
//                // When user changed the Text
//                tripDetailsHistoryListAdapter.
//            }
//
//            @Override
//            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
//                                          int arg3) {
//                // TODO Auto-generated method stub
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable arg0) {
//                // TODO Auto-generated method stub
//            }
//        });
        inputSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                tripDetailsHistoryListAdapter.getFilter().filter(newText);
                return false;
            }
        });

        return rootView;
    }

    private void initializeViewComponents() {

        tripDetailsHistoryRecyclerView = (RecyclerView) rootView.findViewById(R.id.book_ticket_recycler_view);
        tripDetailsHistoryRecyclerView.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        tripDetailsHistoryRecyclerView.setLayoutManager(linearLayoutManager);
        tripUpdateDetailsLists = new ArrayList<>();
        inputSearch=(SearchView)rootView.findViewById(R.id.inputSearch);

        VideoItem v = new VideoItem();
        v.setId("1");
        v.setName("Saleh Al Fawzan");
        VideoItem v2 = new VideoItem();
        v2.setId("2");
        v2.setName("Saleh Al Uthaymeen`");

        VideoItem v3 = new VideoItem();
        v3.setId("3");
        v3.setName("Abdul Aziz Bin Baz");
        VideoItem v4 = new VideoItem();
        v4.setId("4");
        v4.setName("Abdur Rahman Al Sudais");

        VideoItem v5 = new VideoItem();
        v5.setId("5");
        v5.setName("Imam Anwar Al Awlaki");
        VideoItem v6 = new VideoItem();
        v6.setId("6");
        v6.setName("Abdulllah Jahangir");

        VideoItem v7 = new VideoItem();
        v7.setId("7");
        v7.setName("Dr. Saifullah");
        VideoItem v8 = new VideoItem();
        v8.setId("8");
        v8.setName("Nouman Ali Khan");

        VideoItem v9 = new VideoItem();
        v9.setId("9");
        v9.setName("Sheikh Ahmad Musa Jibril");
        VideoItem v10 = new VideoItem();
        v10.setId("10");
        v10.setName("Muhammad Naseel Shahrukh");

        VideoItem v11 = new VideoItem();
        v11.setId("11");
        v11.setName("Manzur- E- Elahi");
        VideoItem v12 = new VideoItem();
       v12.setId("12");
        v12.setName("Saleh Al Munajjid");

        VideoItem v13 = new VideoItem();
        v13.setId("13");
        v13.setName("Sheikh Dr. Bilal Philips");

        VideoItem v14 = new VideoItem();
        v14.setId("14");
        v14.setName("Sheikh Mohammed Salah");
//        VideoItem v10 = new VideoItem();
//        v10.setId("10");
//        v10.setName("Abdur-Raheem Green");
//
//        VideoItem v11 = new VideoItem();
//        v11.setId("11");
//        v11.setName("Manzur- E- Elahi");
//        VideoItem v12 = new VideoItem();
//        v12.setId("12");
//        v12.setName("Abdur Rahman Al Sudais");

        tripUpdateDetailsLists.add(v);
        tripUpdateDetailsLists.add(v2);
        tripUpdateDetailsLists.add(v3);
        tripUpdateDetailsLists.add(v4);
        tripUpdateDetailsLists.add(v5);
        tripUpdateDetailsLists.add(v6);
        tripUpdateDetailsLists.add(v7);
        tripUpdateDetailsLists.add(v8);
        tripUpdateDetailsLists.add(v9);
        tripUpdateDetailsLists.add(v10);
        tripUpdateDetailsLists.add(v11);
        tripUpdateDetailsLists.add(v12);
        tripUpdateDetailsLists.add(v13);
        tripUpdateDetailsLists.add(v14);

        Collections.sort(tripUpdateDetailsLists, new Comparator<VideoItem>(){
            public int compare(VideoItem emp1, VideoItem emp2) {
                // ## Ascending order
                return emp1.getName().compareToIgnoreCase(emp2.getName()); // To compare string values
                // return Integer.valueOf(emp1.getId()).compareTo(emp2.getId()); // To compare integer values

                // ## Descending order
                // return emp2.getFirstName().compareToIgnoreCase(emp1.getFirstName()); // To compare string values
                // return Integer.valueOf(emp2.getId()).compareTo(emp1.getId()); // To compare integer values
            }
        });
        tripDetailsHistoryListAdapter = new TripListAdapter(getActivity(), tripUpdateDetailsLists);
        tripDetailsHistoryRecyclerView.setVisibility(View.VISIBLE);
        tripDetailsHistoryRecyclerView.setAdapter(tripDetailsHistoryListAdapter);
      //  totalTripListTextView = (TextView) rootView.findViewById(R.id.total_trip_list_text_view);
        //  tripDetailsHistoryProgressbar = (ProgressBar) findViewById(R.id.book_ticket_progress_bar);
      //  tripDetailsHistoryView = (View) rootView.findViewById(R.id.book_ticket_view);
//        if(tripUpdateDetailsLists.size()!=0) {
//            tripDetailsHistoryView.setVisibility(VISIBLE);
//        }

        mGestureDetector = new GestureDetector(getActivity(), new GestureDetector.SimpleOnGestureListener() {

            @Override
            public boolean onSingleTapUp(MotionEvent e) {


                return true;
            }
        });


        tripDetailsHistoryRecyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                View child = tripDetailsHistoryRecyclerView.findChildViewUnder(e.getX(), e.getY());
                mGestureDetector.setIsLongpressEnabled(true);
                ProgressDialog  progressDialog =new ProgressDialog(getActivity());
                if (child != null && mGestureDetector.onTouchEvent(e)) {
                    progressDialog.show();
                    int position = tripDetailsHistoryRecyclerView.getChildAdapterPosition(child);
                    VideoItem tripsItem = tripUpdateDetailsLists.get(position);

                    Log.d(TAG, "Position:" + position + " and tempTrips size: " + tripUpdateDetailsLists.size());

                    Intent intent = new Intent(Intent.ACTION_SEARCH);
                    intent.setPackage("com.google.android.youtube");
                    intent.putExtra("query", tripsItem.getName());
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);

                    progressDialog.cancel();


                }

                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {
                //  Toast.makeText(MainActivity.this,"Hi",Toast.LENGTH_LONG).show();
            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
                //Toast.makeText(MainActivity.this,"onRequestDisallowInterceptTouchEvent",Toast.LENGTH_LONG).show();
            }
        });

    }
}
