package honesty.android.islamiclectures;

import android.app.ActionBar;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TabWidget;

public class MainActivity extends AppCompatActivity {

    private String[] tabs;
    private ViewPager viewPager;
    private TabWidget tabWidget;
    private TabLayout tabLayout;
    private Toolbar toolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        Toolbar actionBarToolbar = (Toolbar)MainActivity.this.findViewById(R.id.action_bar);
//        if (actionBarToolbar != null)
//            actionBarToolbar.setTitleTextColor(Color.RED);


        tabLayout = (TabLayout) findViewById(R.id.tabs);

        ViewPager pager = (ViewPager) findViewById(R.id.pager);

        TripHistoryTabsPagerAdapter adapter = new TripHistoryTabsPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);
        pager.setCurrentItem(0);


        tabLayout.setBackgroundColor(Color.parseColor("#7792FC"));
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#ffffff"));
        tabLayout.setTabTextColors(Color.parseColor("#000000"), Color.parseColor("#ffffff"));


        tabLayout.setupWithViewPager(pager);
    }
}
