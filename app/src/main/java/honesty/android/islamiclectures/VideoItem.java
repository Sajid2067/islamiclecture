package honesty.android.islamiclectures;

import android.widget.ImageView;

/**
 * Created by TT on 10/8/2016.
 */

public class VideoItem {

    private String name;
    private String thumbnailURL;
    private String id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getThumbnailURL() {
        return thumbnailURL;
    }

    public void setThumbnailURL(String thumbnailURL) {
        this.thumbnailURL = thumbnailURL;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
